# Beaglebone Setup

Beaglebone wireless setup notes

## Power up
1. Connect to usb on linux computer
2. Wait for power up

## SSH
1. Generate SSH key
    ```bash
    ssh-keygen -t rsa -b 4096 -C "YOUR EMAIL" -f /home/USER/.ssh/id_rsa_bone
    ```
2. Add entry to `~/.ssh/config`
    ```bash
    host bone
        User debian
        Hostname 192.168.7.2
        IdentityFile ~/.ssh/id_rsa_bone
    ```
3. SSH root to print temp password
    ```bash
    ssh root@192.168.7.2
    ```
4. Copy the key
    ```bash
    ssh-copy-id -i ~/.ssh/id_rsa_bone bone
    ```
5. SSH into the beaglebone
    ```bash
    ssh bone
    ```
6. Change password
    ```bash
    passwd
    ```

## Setup Wifi connection
1. Run connection manager
    ```bash
    sudo connmanctl
    ```
2. In the connection manager add a new connection
    ```bash
    enable wifi
    scan wifi
    services
    # pick the service that you want to connect to.
    # e.g wifi_506583d4fc5e_544e434150413937414239_managed_psk
    agent on
    connect wifi_506583d4fc5e_544e434150413937414239_managed_psk
    # enter passphrase
    quit
    ```
3. Verify there's a connection extablished
    ```
    ifconfig
    ```
